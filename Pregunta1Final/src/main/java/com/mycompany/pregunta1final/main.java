/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pregunta1final;

import javax.swing.JOptionPane;

/**
 *
 * @author Jherson Rodrigo Mamani Poma
 * 1- Realizar una estructura de pila de caracteres para comprobar palaba es un palíndromo o no
 */
public class main {
    public static void main(String[] args){
        String x,reversep;
        x = JOptionPane.showInputDialog("Escriba una palabra");
        pila pd = new pila();
        for(int i=0;i<x.length();i++){
            pd.push(x.charAt(i));
        }
        reversep = "";
        
        while(!pd.isEmpty()){
            reversep = reversep+pd.pop();
        }
        
        if(x.equals(reversep)){
            JOptionPane.showMessageDialog(null,"Si es palindromo");
        }else{
            JOptionPane.showMessageDialog(null,"No es palindromo");
        }
    }
}
