/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pregunta2final;
import java.util.Scanner;
/**
 *
 * @author Jherson Rodrigo Mamani Poma
 * 2- Realizar una estructura de cola para que reciba puro enteros y realizar una suma con todos los números que tenga almacenados
 */
public class main {
    public static void main(String[] args){
        Scanner n = new Scanner(System.in);
        int cantidadN, total, i, nuevoN;
        
        System.out.print("Ingrese la cantidad de numeros a sumar: ");
        cantidadN = n.nextInt();  
        total=0;
        
        for(i=1;i<=cantidadN;i++){
            System.out.print("Ingrese el numero(" + i + ") : ");
            nuevoN = n.nextInt();
            total = total + nuevoN;
        }
        System.out.print("La suma total de los numeros es: " +  total);
    }
}