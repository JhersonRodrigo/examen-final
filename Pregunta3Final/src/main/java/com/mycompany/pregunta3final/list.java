/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pregunta3final;

/**
 *
 * @author Jherson Rodrigo Mamani Poma
 */
public class list<X> {
    private nodo<X> first;
    private nodo<X> last;
    private int size;
    
    public list(){
        this.first = null;
        this.last = null;
        this.size = 0;
    }
    public boolean isEmpty(){
        return size == 0;
    }
    public int sizeList(){
        return size;
    }
    private nodo<X> getNode(int index){
        if(isEmpty() || (index<0 || index >=sizeList())){
            return null;
        }else if(index == 0){
            return first;
        }else if(index == sizeList() -1){
            return last;
        }else{
            nodo<X> searchNodo = first;
            int count = 0;
            while(count < index){
                count++;
                searchNodo = searchNodo.getNext();
            }
            return searchNodo;
        }
    }
     public X get(int index){
        if(isEmpty() || (index<0 || index >=sizeList())){
            return null;
        }else if(index == 0){
            return first.getvalue();
        }else if(index == sizeList() -1){
            return last.getvalue();
        }else{
            nodo<X> searchNodeValue = getNode(index);
            return searchNodeValue.getvalue();
        }
    }
    
    public X getFirst(){
        if(isEmpty()){
            return null;
        }else{
            return first.getvalue();
        }
    }
    
    public X getLast(){
        if(isEmpty()){
            return null;
        }else{
            return last.getvalue();
        }
    }
    public X addFirst(X value){
        nodo<X> newvalue;
        if(isEmpty()){
            newvalue = new nodo<>(value,null);
            first = newvalue;
            last = newvalue;
        }else{
            newvalue = new nodo<>(value,first);
            first = newvalue;
        }
        size++;
        return first.getvalue();
    }
    
    public X addLast(X value){
        nodo<X> newvalue;
        if(isEmpty()){
            return addFirst(value);
        }else{
            newvalue = new nodo<>(value,null);
            last.setNext(newvalue);
            last = newvalue;
        }
        size++;
        return last.getvalue();
    }
    
    public X add(X value,int index){
        if(index == 0){
            return addFirst(value);
        }else if(index == sizeList()){
            return addLast(value);
        }else if((index < 0 || index >= sizeList()-1)){
            return null;
        }else{
            nodo<X> nodo_prev = getNode(index-1);
            nodo<X> nodo_current = getNode(index);
            nodo<X> newvalue = new nodo<>(value,nodo_current);
            nodo_prev.setNext(newvalue);
            size++;
            return getNode(index).getvalue();
        }
    }
    
    public X removeFirst(){
        if(isEmpty()){
            return null;
        }else{
            X value = first.getvalue();
            nodo<X> aux = first.getNext();
            first = aux;
            if(sizeList() == 1){
                last = null;
            }
            size--;
            return value;
        }
    }
    
    public X removeLast(){
        if(isEmpty()){
            return null;
        }else{
            X value = last.getvalue();
            nodo<X> newLast = getNode(sizeList()-2);
            if(newLast == null){
                last = null;
                if(sizeList() == 2){
                    last = first;
                }else{
                    first = null;
                }
            }else{
                last =  newLast;
                last.setNext(null);
            }
            size--;
            return value;
        }
    }
    
    public X remove(int index){
        if(index == 0){
            return removeFirst();
        }else if(index == sizeList()){
            return removeLast();
        }else if(isEmpty() || (index < 0 || index >= sizeList())){
            return null;
        }else{
            nodo<X> nodo_current = getNode(index);
            nodo<X> nodo_prev = getNode(index - 1);
            nodo<X> nodo_current_next = nodo_current.getNext();
            X value = nodo_current.getvalue();
            nodo_current = null;
            nodo_prev.setNext(nodo_current_next);
            size--;
            return value;
        }
    }
    
    public X modifyValuesOfNode(X newvalue, int index){
        if(isEmpty() || (index < 0 || index >= sizeList())){
            return null;
        }else{
            nodo<X> current_node = getNode(index);
            current_node.setValue(newvalue);
            return current_node.getvalue();
        }
    }
    
    public Integer indexOfValue(X value){
        if(isEmpty()){
            return null;
        }else{
            nodo<X> element = first;
            int index = 0;
            while(element != null){
                if(value == element.getvalue()){
                    return index;
                }
                index++;
                element = element.getNext();
            }
        }
        return null;
    }
    
    public String contentValuesList(){
        String str = "";
        if(isEmpty()){
            str = "Lista Vacia";
        }else{
            nodo<X> out = first;
            while(out != null){
                str = str + out.getvalue()+ " - ";
                out = out.getNext();
            }
        }
        return str;
    }
}
